Crime Prediction - Data Science 3rd Place in Class Competition

In this competition your goal is to predict the amount of crime near a location using the street view image of the location. You are to work alone for the competition, using image processing techniques learned in class. You may augment the data set with census variables, but you may not use external crime data nor can you hand label images. Do not post code on kernels, all code should be kept private and not shared with other participants.

The evaluation metric for the competition is Spearman's R coefficient. See https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient

This is a number between -1 and 1 like correlation (higher the better), but uses the rank of your predictions rather than their actual values.